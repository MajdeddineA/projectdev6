//https://leafletjs.com/index.html
//https://leaflet-extras.github.io/leaflet-providers/preview/


var timee = document.getElementById('time');
timee.style.display = 'none';

var plan = document.getElementById('plan');
plan.style.display = 'none';

function createTime() {
    timee.style.display = 'block';
    plan.style.display = 'none';
}

function createPlan() {
    timee.style.display = 'none';
    plan.style.display = 'block';


    // call out all station
    $(document).ready(() => {
        $.ajax({
            url: "http://data.metromobilite.fr/api/routers/default/index/routes",
            type: "GET",
            dataType: "json",
            success: function (res, status, xhr) {
                console.log(res)
    
                let output20 = '';
                let output21 = '';
                let output22 = '';
                let output23 = '';
                
                for (let i = 0; i < res.length; i++) {
                    if (res[i].type === "TRAM") {
                        $('#h4').html(res[i].type);
                        output20 += `
                            
                            <div class="col-sm-2">
                                
    
                                <a onclick="stationSelected2('${res[i].id}')" id="${res[i].id}" class="svgLigne pull-left" title="${res[i].shortName - res[i].longName}" href="#map">
                                <svg id="svg_${res[i].id}" data-code="${res[i].id}" class="logo" style="font-size:60px;font-family: Arial;font-weight:bold;text-anchor: middle;stroke-width:6px;" viewBox="0 0 100 100">		<title>${res[i].shortName} - ${res[i].longName}</title>		<circle class="svgFond" cx="50" cy="50" r="45" stroke="white" stroke-width="0" fill="#${res[i].color}"></circle>				<text class="svgNumLigne" x="50%" y="72.5%" fill="#${res[i].textColor}">${res[i].shortName}</text>	</svg></a>
                                 
                            </div>
                          `;
                    }
                    else if (res[i].type === "CHRONO") {
                        $('#h5').html(res[i].type);
                        output21 += `
                          
                        <div class="col-sm-2">
                                
    
                            <a onclick="stationSelected2('${res[i].id}')" id="${res[i].id}" class="svgLigne pull-left" title="${res[i].shortName - res[i].longName}" href="#map">
                            <svg id="svg_${res[i].id}" data-code="${res[i].id}" class="logo" style="font-size:60px;font-family: Arial;font-weight:bold;text-anchor: middle;stroke-width:6px;" viewBox="0 0 100 100">		<title>${res[i].shortName} - ${res[i].longName}</title>		<circle class="svgFond" cx="50" cy="50" r="45" stroke="white" stroke-width="0" fill="#${res[i].color}"></circle>				<text class="svgNumLigne" x="50%" y="72.5%" fill="#${res[i].textColor}">${res[i].shortName}</text>	</svg></a>
                         
                        </div>
                        `;
                    }
                    else if (res[i].type === "PROXIMO") {
                        $('#h6').html(res[i].type);
                        output22 += `
                          
                        <div class="col-sm-2">
                                
    
                        <a onclick="stationSelected2('${res[i].id}')" id="${res[i].id}" class="svgLigne pull-left" title="${res[i].shortName} - ${res[i].longName}" href="#map">
                        <svg id="svg_${res[i].id}" data-code="${res[i].id}" class="logo" style="font-size:60px;font-family: Arial;font-weight:bold;text-anchor: middle;stroke-width:6px;" viewBox="0 0 100 100">		<title>${res[i].shortName} - ${res[i].longName}</title>				<rect class="svgFond" x="5" y="10" width="90" stroke="white" stroke-width="0" height="80" rx="10" ry="10" fill="#${res[i].color}"></rect>		<text class="svgNumLigne" x="50%" y="72.5%" fill="#${res[i].textColor}">${res[i].shortName}</text>	</svg></a>
                         
                        </div>
                        `;
                    }
                    else if (res[i].type === "FLEXO") {
                        $('#h7').html(res[i].type);
                        output23 += `
                          
                        <div class="col-sm-2">
                                
    
                        <a onclick="stationSelected2('${res[i].id}')" id="${res[i].id}" class="svgLigne pull-left" title="${res[i].shortName} - ${res[i].longName}" href="#map">
                        <svg id="svg_${res[i].id}" data-code="${res[i].id}" class="logo" style="font-size:60px;font-family: Arial;font-weight:bold;text-anchor: middle;stroke-width:6px;" viewBox="0 0 100 100">		<title>${res[i].shortName} - ${res[i].longName}</title>				<rect class="svgFond" x="5" y="10" width="90" stroke="white" stroke-width="0" height="80" rx="10" ry="10" fill="#${res[i].color}"></rect>		<text class="svgNumLigne" x="50%" y="72.5%" fill="#${res[i].textColor}">${res[i].shortName}</text>	</svg></a>
                         
                        </div>
                        `;
                    }
                }
    
                $('#list20').html(output20);
                $('#list21').html(output21);
                $('#list22').html(output22);
                $('#list23').html(output23);
    
                check = 'true';
                
            },
            error: function (xhr, status, error) {
                $("#message").html("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
            }
        });
      });
    
      // call out the map with grenoble coordinates
      var map = L.map('map').setView([45.16667, 5.71667], 12);

      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    
    // plan de reseaux for detect station
    function stationSelected2(id) {

        $.ajax({
            url: "https://data.metromobilite.fr/api/lines/json?types=ligne&codes="+ id,
            type: "GET",
            dataType: "json",
            success: function (res, status, xhr) {
                /* eslint no-console: "error" */
                console.log(res)
                
                L.geoJSON(res, {
                    style: function (feature) {
                        var col = 'rgb' + '(' + feature.properties.COULEUR + ')';
                        //console.log(col)
    
                        function componentFromStr(numStr, percent) {
                            var num = Math.max(0, parseInt(numStr, 10));
                            return percent ?
                                Math.floor(255 * Math.min(100, num) / 100) : Math.min(255, num);
                        }
                        
                        function rgbToHex(rgb) {
                            var rgbRegex = /^rgb\(\s*(-?\d+)(%?)\s*,\s*(-?\d+)(%?)\s*,\s*(-?\d+)(%?)\s*\)$/;
                            var result, r, g, b, hex = "";
                            if ( (result = rgbRegex.exec(rgb)) ) {
                                r = componentFromStr(result[1], result[2]);
                                g = componentFromStr(result[3], result[4]);
                                b = componentFromStr(result[5], result[6]);
                            
                                hex = "#" + (0x1000000 + (r << 16) + (g << 8) + b).toString(16).slice(1);
                            }
                            return hex;
                        }
                        
                        rgbToHex(col);
    
                        return {color: col};
                        
                    }
    
    
                }).addTo(map);    
          }
      }),

      $.ajax({
        url: "https://data.metromobilite.fr/api/ficheHoraires/json?route="+ id,
        type: "GET",
        dataType: "json",
        success: function (userData, status, xhr) {
            /* eslint no-console: "error" */
            console.log(userData)

            for (i=0 ; i < userData[0].arrets.length ; i++){
                L.marker([userData[0].arrets[i].lat, userData[0].arrets[i].lon]).bindPopup(userData[0].arrets[i].stopName).addTo(map)
            }

        }
    });


    }
    // eslint solve "not used function error"
    window.stationSelected2 = stationSelected2;
    
}

         //call out all station
         $(document).ready(() => {
            $.ajax({
                url: "http://data.metromobilite.fr/api/routers/default/index/routes",
                type: "GET",
                dataType: "json",
                success: function (res, status, xhr) {
                    console.log(res)
    
                    let output = '';
                    let output1 = '';
                    let output2 = '';
                    let output3 = '';
                    
                    for (let i = 0; i < res.length; i++) {
                        if (res[i].type === "TRAM") {
                            $('#h').html(res[i].type);
                            output += `
                                
                                <div class="after col-sm-2">
                                    
        
                                    <a onclick="stationSelected('${res[i].id}')" id="${res[i].id}" class="svgLigne pull-left" title="${res[i].shortName - res[i].longName}" href="#nextprev">
                                    <svg id="svg_${res[i].id}" data-code="${res[i].id}" class="logo" style="font-size:60px;font-family: Arial;font-weight:bold;text-anchor: middle;stroke-width:6px;" viewBox="0 0 100 100">		<title>${res[i].shortName} - ${res[i].longName}</title>		<circle class="svgFond" cx="50" cy="50" r="45" stroke="white" stroke-width="0" fill="#${res[i].color}"></circle>				<text class="svgNumLigne" x="50%" y="72.5%" fill="#${res[i].textColor}">${res[i].shortName}</text>	</svg></a>
                                     
                                </div>
                              `;
                        }
                        else if (res[i].type === "CHRONO") {
                            $('#h1').html(res[i].type);
                            output1 += `
                              
                            <div class="after col-sm-2">
                                    
        
                                <a onclick="stationSelected('${res[i].id}')" id="${res[i].id}" class="svgLigne pull-left" title="${res[i].shortName - res[i].longName}" href="#nextprev">
                                <svg id="svg_${res[i].id}" data-code="${res[i].id}" class="logo" style="font-size:60px;font-family: Arial;font-weight:bold;text-anchor: middle;stroke-width:6px;" viewBox="0 0 100 100">		<title>${res[i].shortName} - ${res[i].longName}</title>		<circle class="svgFond" cx="50" cy="50" r="45" stroke="white" stroke-width="0" fill="#${res[i].color}"></circle>				<text class="svgNumLigne" x="50%" y="72.5%" fill="#${res[i].textColor}">${res[i].shortName}</text>	</svg></a>
                             
                            </div>
                            `;
                        }
                        else if (res[i].type === "PROXIMO") {
                            $('#h2').html(res[i].type);
                            output2 += `
                              
                            <div class="after col-sm-2">
                                    
        
                            <a onclick="stationSelected('${res[i].id}')" id="${res[i].id}" class="svgLigne pull-left" title="${res[i].shortName} - ${res[i].longName}" href="#nextprev">
                            <svg id="svg_${res[i].id}" data-code="${res[i].id}" class="logo" style="font-size:60px;font-family: Arial;font-weight:bold;text-anchor: middle;stroke-width:6px;" viewBox="0 0 100 100">		<title>${res[i].shortName} - ${res[i].longName}</title>				<rect class="svgFond" x="5" y="10" width="90" stroke="white" stroke-width="0" height="80" rx="10" ry="10" fill="#${res[i].color}"></rect>		<text class="svgNumLigne" x="50%" y="72.5%" fill="#${res[i].textColor}">${res[i].shortName}</text>	</svg></a>
                             
                            </div>
                            `;
                        }
                        else if (res[i].type === "FLEXO") {
                            $('#h3').html(res[i].type);
                            output3 += `
                              
                            <div class="after col-sm-2">
                                    
        
                            <a onclick="stationSelected('${res[i].id}')" id="${res[i].id}" class="svgLigne pull-left" title="${res[i].shortName} - ${res[i].longName}" href="#nextprev">
                            <svg id="svg_${res[i].id}" data-code="${res[i].id}" class="logo" style="font-size:60px;font-family: Arial;font-weight:bold;text-anchor: middle;stroke-width:6px;" viewBox="0 0 100 100">		<title>${res[i].shortName} - ${res[i].longName}</title>				<rect class="svgFond" x="5" y="10" width="90" stroke="white" stroke-width="0" height="80" rx="10" ry="10" fill="#${res[i].color}"></rect>		<text class="svgNumLigne" x="50%" y="72.5%" fill="#${res[i].textColor}">${res[i].shortName}</text>	</svg></a>
                             
                            </div>
                            `;
                        }
                    }
    
                    $('#list').html(output);
                    $('#list1').html(output1);
                    $('#list2').html(output2);
                    $('#list3').html(output3);
    
                    check = 'true';
                    
                },
                error: function (xhr, status, error) {
                    $("#message").html("Result: " + status + " " + error + " " + xhr.status + " " + xhr.statusText)
                }
            });
          });
          
          //to pick up the time and the date form the datetimepicker1
          function time() {
              //console.log($("#datetimepicker").find("input").val())
            //console.log($("#datetimepicker1").find("input").val() + " " + $("#datetimepicker").find("input").val());
    
            let ds = $("#datetimepicker1").find("input").val();
            let ts = $("#datetimepicker").find("input").val();
    
            let convert = ds + "T" + ts + '+' + '00:00';
            //console.log(convert);
    
            //console.log(Math.round(new Date($("#datetimepicker1").find("input").val()).getTime($("#datetimepicker").find("input").val())/1000))
    
            var timedatepiker = Math.round(new Date(convert).getTime()/1000) + '000';
            //console.log(timedatepiker)
            return timedatepiker;
          }
    
            let output4 = '';
            let output5 = '';
            let output6 = '';
    
            var check = 'true';
    
            var test = '';
            var oldid = '';
    
    
        // time detect for selected station
        function stationSelected(id) {
    
            if (oldid !== id) {// for check if the id changed then get the time data from datetimepiker
                check = 'true';
            }
    
            oldid = id;
    
            if (check === 'true') {
                test=time();
                console.log(test)
                console.log(check)
            } else if (check === 'false') {
                //test=prevtime();
                test = pretime;
                //setTimeout(() => test=prevtime(), 1000);
                console.log(test)
                console.log(check)
            } else if (check === 'trfal') {
    
                test = nexTime;
                console.log(test)
                console.log(check)
            }
            console.log(id)
            console.log(oldid)
    
            $("#staionselected").html('');
    
            $.ajax({
                url: "https://data.metromobilite.fr/api/ficheHoraires/json?route="+ id + "&time=" + test,
                //url: "https://data.metromobilite.fr/api/ficheHoraires/json?route="+ id,
                type: "GET",
                dataType: "json",
                success: function (res, status, xhr) {
                    /* eslint no-console: "error" */
                    console.log(res)
                    //console.log(id)
                    //console.log(res[0].arrets[1].trips)
                    //console.log($("#datetimepicker").data("datetimepicker").getDate()) 
    
                    var formattedTime = '';
                    let i = 0;
                    let j = 0;
                    //console.log('00000' + res[0].arrets[0].trips[0]);
    
                    for (i = 0; i < res[0].arrets.length; i++) {
                        for (j = 0; j < res[0].arrets[i].trips.length; j++) {
    
                            unixTimestamp = res[0].arrets[i].trips[j]; 
      
                            // convert to milliseconds 
                            // and then create a new Date object 
                            dateObj = new Date(unixTimestamp * 1000); 
                  
                            // Get hours from the timestamp 
                            hours = dateObj.getUTCHours(); 
                  
                            // Get minutes part from the timestamp 
                            minutes = dateObj.getUTCMinutes(); 
                  
                            // Get seconds part from the timestamp 
                            seconds = dateObj.getUTCSeconds(); 
                  
                            formattedTime += hours.toString().padStart(2, '0') + ':' + 
                                minutes.toString().padStart(2, '0') + "&nbsp";
    
                            
                        }
                        //console.log(formattedTime);
                        
                        for (let i = 0; i < res[0].arrets.length; i++) {
                            var lastArray = res[0].arrets[i].stopName;
                        }
                        //console.log(lastArray)
    
                        var prevTime = res[0].prevTime;
                        var nextTime = res[0].nextTime;
    
                        output5 = id;
                        /*output5 = `
                                    <div data-code="${id}" class="ligne onglet" style="background-color: rgb(71, 154, 69); color: rgb(255, 255, 255);">
                                        <span class="modeLigne"><svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 48 48" style="fill: rgb(255, 255, 255);"><path d="M38,33.88V17c0-5.58-5.221-6.8-12.02-6.98L27.5,7H34V4H14v3h9.5l-1.52,3.04C15.72,10.22,10,11.46,10,17v16.88c0,2.9,2.38,5.32,5.18,5.94L12,43v1h4.46l4-4H28l4,4h4v-1l-3-3h-0.16C36.221,40,38,37.26,38,33.88z M24,37c-1.66,0-3-1.34-3-3s1.34-3,3-3s3,1.34,3,3S25.66,37,24,37z M34,28H14V18h20V28z"></path></svg></span>
                                        <span class="numLigne">B</span>
                                    </div>
                                    `;*/
    
                        output6 = `
                                    <a onclick="prevtime('${prevTime}', '${id}');" class="btn btn-horaires horairesPrecedents" title="Affiche les 4 horaires précédents" alt="Affiche les 4 horaires précédents" href="#nextprev"><svg width="20px" height="20px" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M6 6h2v12H6zm3.5 6l8.5 6V6z"></path></svg></a>
                                    <a onclick="nexttime('${nextTime}', '${id}');" href="#nextprev" class="btn btn-horaires horairesSuivants" data-time="${res[0].nextTime}" title="Affiche les 4 horaires suivants" alt="Affiche les 4 horaires suivants"><svg width="20px" height="20px" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M6 18l8.5-6L6 6v12zM16 6v12h2V6h-2z"></path></svg></a>
                                    <span><span class="directionLabel">Direction: </span><span class="directionText">${lastArray}</span></span>
                                    
                                    <!--<div class="boutonDirection">
                                        <a onclick="replaseStationOrder('$');" href="#" class="btn btn-horaires ficheHoraireDirection" data-codeligne="id"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="30px" height="30px" viewBox="0 0 48 48">
                                        <title>Changement de direction</title>
                                        <path d="M16,13.98V28h4V13.98h6L18,6l-8,7.98H16z M30,42l8-7.98h-6V20h-4v14.02h-6L30,42z"></path>
                                        </svg></a>
                                        <span><span class="directionLabel">Direction: </span><span class="directionText">${lastArray}</span></span>
                                    </div>-->
                                    `;
    
                
                        output4 += `
                              
                                <tr data-lonlat="${res[0].arrets[i].lat},${res[0].arrets[i].lon}" data-arr-code="${res[0].arrets[i].stopId}" data-parent-station="SEM:GENVALLCATA">
                                    <td> <span></span></td>
                                    <td title="Grenoble"><span class="glyphicon glyphicon-screenshot" style="display:none;"></span>${res[0].arrets[i].stopName}</td>
                                    <td title="26/08/2020" alt="26/08/2020">${formattedTime}</td>
                                </tr>
                            `;                          
    
                        formattedTime = '';// for empty the array time in every station
                        
                    }
                    $('#staionselected1').html(output5);
                    $('#nextprev').html(output6);
              $('#staionselected').html(output4);
    
              output4 = '';
    
              }
          });
      }
      // eslint solve "not used function error"
      window.stationSelected = stationSelected;
    
      
      //previous time for any choosen transport
      var pretime = '';
      function prevtime(prevTime, id) {
        check = 'false';
        pretime = prevTime;
        console.log(pretime)
        stationSelected(id);
        return pretime;
      }
    
    
      //next time for any choosen transport
      var nexTime = '';
      function nexttime(nextTime, id) {
        check = 'trfal';
        nexTime = nextTime;
        console.log(nexTime)
        stationSelected(id);
        return nexTime;
      }
    
    
      
    
      // for detect the time
      $(function() {
        $('#datetimepicker').datetimepicker({
          pickDate: false,
          //timeFormat: 'hh:mm',
          pickSeconds: true
        });
      });
    
      //for detect the date
      $(function() {
        $('#datetimepicker1').datetimepicker({
          pickTime: false
        });
      });    