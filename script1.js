function stationSelected2(id) {
    fetch(`https://data.metromobilite.fr/api/lines/json?types=ligne&codes=`+ id)
    .then((response) => {
      /* eslint no-console: "error" */
      // console.log(response);
      if (response.ok === true) {
        return response.json().then((res) => {
          /* eslint no-console: "error" */
          console.log(res);

          L.geoJSON(res, {
            style: function (feature) {
                var col = 'rgb' + '(' + feature.properties.COULEUR + ')';
                //console.log(col)

                function componentFromStr(numStr, percent) {
                    var num = Math.max(0, parseInt(numStr, 10));
                    return percent ?
                        Math.floor(255 * Math.min(100, num) / 100) : Math.min(255, num);
                }
                
                function rgbToHex(rgb) {
                    var rgbRegex = /^rgb\(\s*(-?\d+)(%?)\s*,\s*(-?\d+)(%?)\s*,\s*(-?\d+)(%?)\s*\)$/;
                    var result, r, g, b, hex = "";
                    if ( (result = rgbRegex.exec(rgb)) ) {
                        r = componentFromStr(result[1], result[2]);
                        g = componentFromStr(result[3], result[4]);
                        b = componentFromStr(result[5], result[6]);
                    
                        hex = "#" + (0x1000000 + (r << 16) + (g << 8) + b).toString(16).slice(1);
                    }
                    return hex;
                }
                
                rgbToHex(col);

                return {color: col};
                
            }


        }).addTo(map);  



          return fetch(`https://data.metromobilite.fr/api/ficheHoraires/json?route=`+ id);
        }).then((response) => {
          if (response.ok) {
            return response.json();
          }
        }).then((userData) => {
          console.log(userData);
          

          for (i=0 ; i < userData[0].arrets.length ; i++){
            L.marker([userData[0].arrets[i].lat, userData[0].arrets[i].lon]).bindPopup(userData[0].arrets[i].stopName).addTo(map)
        }


        });
      }
      return false;
    });
  }


// eslint solve "not used function error"
window.stationSelected2 = stationSelected2;